/* eslint-disable no-unused-expressions */
import { fixture, assert } from "@open-wc/testing";

import "../sellfone-product-carousel.js";

describe("Suite cases", () => {
  it("Case default", async () => {
    const _element = await fixture("<sellfone-product-carousel></sellfone-product-carousel>");
    assert.strictEqual(_element.hello, 'Hello World!');
  });
});

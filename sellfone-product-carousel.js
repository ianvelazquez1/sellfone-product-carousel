import { html, LitElement } from 'lit-element';
import style from './sellfone-product-carousel-styles.js';

class SellfoneProductCarousel extends LitElement {
  static get properties() {
    return {
      currentUrl:{
        type:String,
        attribute:"current-url"
      },
       imagesArray:{
         type:Array,
         attribute:"images-array"
       }
    };
  }

  static get styles() {
    return style;
  }

  constructor() {
    super();
    this.imagesArray=[];
  }

  render() {
    return html`
        <div class="mainContainer">
           <div class="imagesContainer">
              <img class="carouselImage" src="${this.currentUrl}" alt="">
             <!-- <button class="seeMoreButton">See more</button>-->
           </div>
           <div class="subMainContainer">
              
                ${this.imagesArray.map((image,index)=>html`<div class="miniImagesContainer" @click="${(e)=>this._selectImage(index)}" ><img class="miniImage" alt= "" src="${image}"></div>`)}
           </div>
        </div>
      `;
    }

    updated(updatedProperties){
      if(updatedProperties.has("imagesArray")){
        this.currentUrl=this.imagesArray[0];
      }
    }

    _selectImage(event){
      this.currentUrl=this.imagesArray[event];
      let images=this.shadowRoot.querySelectorAll(".miniImagesContainer");
      images.forEach(element=>{
        element.classList.remove("miniImagesContainerSelected")
      });
      images[event].classList.toggle("miniImagesContainerSelected");

    }
}

window.customElements.define("sellfone-product-carousel", SellfoneProductCarousel);
